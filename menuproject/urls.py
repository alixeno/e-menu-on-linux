from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('emenu/django-admin/', admin.site.urls),
    path('', include("menuapp.urls")),

]


urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
#fetching static files to html files
urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
