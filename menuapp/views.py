from django.views.generic import *
from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.urls import reverse_lazy, reverse
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail, send_mass_mail
from urllib.parse import urlparse, parse_qs
from django.http import JsonResponse


#Api imports
from rest_framework.generics import ListCreateAPIView
from .serializers import StaffSerializer


class StaffApiView(ListCreateAPIView):
	queryset = Staff.objects.all()
	serializer_class = StaffSerializer




# Client BaseMixin View
class BaseMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["restro"] = Restaurant.objects.all()
		context["allsliders"] = SlideBanner.objects.all().order_by("-id")
		context["allitems"] = Item.objects.all().order_by("-id")
		context["allspecialitems"] = SpecialItem.objects.all().order_by("-id")
		context["allmenus"] = Menu.objects.all().order_by("-id")
		context["allstaffs"] = Staff.objects.all().order_by("-id")
		context["allfeeds"] = Feedback.objects.all().order_by("-id")
		context["subsform"] = SubscriberForm
		context["cform"] = CustomerSignupForm
		context['allalbum'] = Album.objects.all()
		context['allalbumimages'] = AlbumImage.objects.all()
		context['allvidoes'] = AlbumVideo.objects.all()

		# Assigning customer to recently created or already present cart id
		basket_id = self.request.session.get("mybasket", None)
		logged_in_user = self.request.user
		if logged_in_user.is_authenticated and logged_in_user.groups.filter(name = "customer").exists():
			customer = Customer.objects.get(user=logged_in_user)
		if basket_id:
			basket_obj = Basket.objects.get(id=basket_id)
			basket_obj.customer = customer
			basket_obj.save()

		return context



# Client CustomerRequiredMixin View
class CustomerRequiredMixin(object):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated and request.user.groups.filter(name="customer").exists():
			pass
		else:
			return redirect("menuapp:customerlogin")

		return super().dispatch(request, *args, **kwargs)




# Client CustomerRequiredMixin View
class CustomerRequiredMixin(object):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated and request.user.groups.filter(name="customer").exists():
			pass
		else:
			return redirect("ecomapp:customerlogin")

		return super().dispatch(request, *args, **kwargs)





# Client Home View
class ClientHomeView(BaseMixin, TemplateView):
	template_name = 'clienttemplates/clienthome.html'




# Client Gallery View
class ClientGalleryView(BaseMixin, TemplateView):
    template_name = "clienttemplates/clientgallery.html"




class ClientAlbumView(BaseMixin, TemplateView):
	template_name = "clienttemplates/clientalbum.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['allalbum'] = Album.objects.all()
		context['allalbumimages'] = AlbumImage.objects.all()
		context['allvidoes'] = AlbumVideo.objects.all()

		return context



class ClientAlbumDetailView(BaseMixin, DetailView):
	template_name = "clienttemplates/clientalbumdetail.html"
	model = Album
	context_object_name = "clientalbumobject"





# Client Service View
class ClientServiceView(BaseMixin, TemplateView):
    template_name = "clienttemplates/clientservice.html"


# Client Enquery View
class ClientEnqueryView(BaseMixin, SuccessMessageMixin, CreateView):
    template_name = "clienttemplates/clientenquery.html"
    form_class = EnqueryForm
    success_url = reverse_lazy("menuapp:clientenquery")

    def get_success_message(self, cleaned_data):

        return "Thankyou for your enquery !!!"

    def form_valid(self, form):

        form_email = form.cleaned_data["email"]
        item = form.cleaned_data["item"]
        print(item)

        subject = "Enquery: Xeno Restro!!!"
        from_email = settings.EMAIL_HOST_USER
        enquery_message = "Thankyou for your queries!!! We will look into it ASAP!!!"
        message = form.cleaned_data["message"]

        datatuple = (
            (subject, message, from_email, [from_email]),
            (subject, enquery_message,
             from_email, [form_email]),
        )
        send_mass_mail(datatuple)

        return super().form_valid(form)



class AjaxGetItemsView(TemplateView):
	template_name = "clienttemplates/ajaxgetitems.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		menu_id = self.request.GET["menu_id"]
		menu_obj = Menu.objects.get(id = menu_id)
		print(menu_obj, "&&&&&&&&&&&&&&&&&&&&")
		items = Item.objects.filter(menu = menu_obj)
		context["relateditems"] = items


		return context



# Client Feedback View
class ClientFeedbackView(BaseMixin, SuccessMessageMixin, CreateView):
    template_name = "clienttemplates/clientfeedback.html"
    form_class = FeedbackForm
    success_url = reverse_lazy("menuapp:clienthome")

    def get_success_message(self, cleaned_data):

        return "Thanks for your Feedback !!!"

    def form_valid(self, form):

        form_email = form.cleaned_data["email"]

        subject = "Contacting Xeno Restro!!!"
        from_email = settings.EMAIL_HOST_USER
        contact_message = "Thankyou for your Valuable Feedback!!! We really appreciate your concern!!!"
        message = "You have got a new contact message"

        datatuple = (
            (subject, message, from_email, [from_email]),
            (subject, contact_message,
             from_email, [form_email]),
        )
        send_mass_mail(datatuple)

        return super().form_valid(form)


# Client Item View
class ClientItemView(BaseMixin, TemplateView):
    template_name = "clienttemplates/clientitem.html"

    def get_context_data(self, **kwargs):
    	context = super().get_context_data(**kwargs)
    	menu = Item.objects.all()

    	page = self.request.GET.get('page',1)

    	paginator = Paginator(menu, 8)
    	try:
    		results = paginator.page(page)
    	except PageNotAnInteger:
    		results = paginator.page(1)
    	except EmptyPage:
    		results = paginator.page(paginator.num_pages)

    	context["allitems"] = results

    	return context

 
# Client Menu Detail View
class ClientMenuDetailView(BaseMixin, DetailView):
	template_name = "clienttemplates/clientmenudetail.html"
	model = Menu
	context_object_name = "allmenu"


# Client Special Item List View
class ClientSpecialItemListView(BaseMixin, TemplateView):
	template_name = "clienttemplates/clientspecialitem.html"


# Client Subscriber Form View
class SubscriberCreateView(BaseMixin, SuccessMessageMixin, CreateView):
    template_name = "clienttemplates/clientsubscriber.html"
    form_class = SubscriberForm
    success_url = "/"

    def get_success_message(self, cleaned_data):

        return "Thankyou for subscribing us!!!"

    def form_valid(self, form):

        form_email = form.cleaned_data["email"]

        subject = 'Site contact form'
        from_email = settings.EMAIL_HOST_USER
        contact_message = "Thanks for subscribing us. We really appreciate your step."
        send_mail(subject,
                  contact_message,
                  from_email,
                  [form_email],
                  fail_silently=False)
        return super().form_valid(form)	


# Client Sidebar View
# class ClientSidebarView(TemplateView):
#     template_name = "clienttemplates/sidebar.html"


class SearchView(TemplateView):
	template_name = "clienttemplates/search.html"

	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['allmenus'] = Menu.objects.all()
		context['allspecials'] = SpecialItem.objects.all()
		keyword = self.request.GET["alias"]
		#keyword = self.request.GET.get("dipak")
		# items = News.objects.filter(title__icontains = keyword)
		items = Item.objects.filter(
			Q(name__icontains = keyword) | 
			Q(menu__title__icontains = keyword) | 
			Q(menu__caption__icontains = keyword))
		context["keyword"] = keyword

		page = self.request.GET.get('page', 1)

		paginator = Paginator(items, 8)
		try:
		    results = paginator.page(page)
		except PageNotAnInteger:
		    results = paginator.page(1)
		except EmptyPage:
		    results = paginator.page(paginator.num_pages)
		#for pagination

		context["allitems"] = results



		print(items, "***********************")
		
		return context





# Client AddToCart View
class AddToBasketView(TemplateView):
	template_name = 'clienttemplates/clientaddtobasket.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		item_id = self.kwargs["pk"]
		item = Item.objects.get(id = item_id)
		basket_id = self.request.session.get("mybasket", None)
		if basket_id:
			mybasket = Basket.objects.get(id=basket_id)
			print("old Basket")
		else:
			mybasket = Basket.objects.create(total=0)
			self.request.session["mybasket"] = mybasket.id
			print("new Basket")


		basketitemquery = mybasket.itembasket_set.filter(item = item) 
		if basketitemquery.exists():
			bitem = basketitemquery.first()
			bitem.quantity += 1
			bitem.subtotal += item.price
			bitem.save()
			mybasket.total += item.price
			mybasket.save()
			print("Item already exists in basket***")
		else:
			bitem = ItemBasket.objects.create(
				basket=mybasket, 
				item=item, 
				rate=item.price, 
				quantity=1, 
				subtotal=item.price)
			mybasket.total += item.price
			mybasket.save()

			print("Item deosnt exists in basket***")


		return context




class BasketView(BaseMixin, TemplateView):
	template_name = "clienttemplates/basket.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		basket_id = self.request.session.get("mybasket", None)
		try:
			basket = Basket.objects.get(id = basket_id)
		except:
			basket = None
		context["mybasket"] = basket

		return context

class OrderListView(LoginRequiredMixin, BaseMixin, ListView):
	template_name = 'clienttemplates/orderlist.html'
	queryset = Order.objects.all()
	context_object_name = 'allorders'

	def get_queryset(self):
		data = super().get_queryset()
		customer = Customer.objects.get(user=self.request.user)
		return data.filter(customer=customer)



class OrderDetailView(LoginRequiredMixin, BaseMixin, DetailView):
	template_name = "clienttemplates/orderdetail.html"
	model = Order
	context_object_name = "orderobject"





# Client Cart Manage View
class ManageBasketView(BaseMixin, TemplateView):
	template_name = 'clienttemplates/clientmanagebasket.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		basketitem_id = self.kwargs["pk"]
		action = self.kwargs["action"]
		basketitem_obj = ItemBasket.objects.get(id = basketitem_id)
		basket = basketitem_obj.basket
		#basket_id = self.request.session.get("mybasket", None)
		#basket = Basket.objects.get(id = basket_id)
		if action == 'inr':
			basketitem_obj.quantity += 1
			basketitem_obj.subtotal += basketitem_obj.rate
			basketitem_obj.save()
			basket.total += basketitem_obj.rate
			basket.save()
			context["message"] = "item '" + basketitem_obj.item.name + "' increased successfully"
		elif action == 'dcr':
			basketitem_obj.quantity -= 1
			basketitem_obj.subtotal -= basketitem_obj.rate
			basketitem_obj.save()
			basket.total -= basketitem_obj.rate
			basket.save()
			if basketitem_obj.quantity == 0:
				basketitem_obj.delete()
			context["message"] = "item '" + basketitem_obj.item.name + "' decreased successfully"
		elif action == 'rmv':
			item_title = basketitem_obj.item.name
			basket.total -= basketitem_obj.subtotal
			basket.save()
			basketitem_obj.delete()
			context["message"] = item_title + " removed from basket successfully"
		else:
			context["message"] = "Invalid operation"

		return context


class EmptyBasketView(BaseMixin, TemplateView):
	template_name = 'clienttemplates/clientmanagebasket.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		c_id = self.request.session.get("mybasket", None)
		if c_id:
			basket_obj = Basket.objects.get(id = c_id)
			basketitems = basket_obj.itembasket_set.all()
			basket_obj.total = 0
			basket_obj.save()
			basketitems.delete()
			context["message"] = 'basket is now empty'
		else:
			context["message"] = "Order first to empty basket..."


		return context





class OrderCreateView(BaseMixin, CustomerRequiredMixin, CreateView):
	template_name = 'clienttemplates/ordercreate.html'
	form_class = OrderForm
	success_url = reverse_lazy("menuapp:clienthome")

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		basket_id = self.request.session.get("mybasket", None)
		basket = Basket.objects.get(id = basket_id)
		context["basket_obj"] = basket

		return context

	def form_valid(self, form):
		basket = Basket.objects.get(id = self.request.session.get("mybasket"))
		form.instance.basket = basket
		form.instance.customer = basket.customer
		form.instance.subtotal = basket.total
		form.instance.discount = 0
		form.instance.total = basket.total
		form.instance.order_status = "orderplaced"
		del self.request.session["mybasket"]
		# basket_id = self.request.session.get("mybasket")
		# del basket_id


		return super().form_valid(form)






# Client Customer Signup View
class CustomerSignupView(BaseMixin, CreateView):
	template_name = 'clienttemplates/customersignup.html'
	form_class = CustomerSignupForm
	success_url = reverse_lazy("menuapp:clienthome")
	def form_valid(self, form):
		uname = form.cleaned_data["username"]
		email = form.cleaned_data["email"]
		password = form.cleaned_data["password"]
		user = User.objects.create_user(uname, email, password)
		form.instance.user = user

		return super().form_valid(form)



# Customer Login View Class
class CustomerLoginView(FormView):
	template_name = 'clienttemplates/customerlogin.html'
	form_class = SigninForm
	success_url = reverse_lazy("menuapp:clienthome")

	def form_valid(self, form):
		uname = form.cleaned_data['username']
		pwrd = form.cleaned_data['password']
		user = authenticate(username=uname, password=pwrd)
		if user is not None:
			login(self.request, user)
		else:
			return render(self.request, self.template_name,{
			'error': 'Invalid Username or Password!!!',
			'form': form
			})
		return super().form_valid(form)

	def get_success_url(self):
		logged_in_user = self.request.user
		if logged_in_user.groups.filter(name = 'customer').exists():
			return reverse("menuapp:clienthome")
		else:
			return reverse("menuapp:customersignup")






#----------------------
#----------------------
#----------------------
#----------------------
#----------------------

# Admin Required Mixin Class View
class AdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("/emenu-admin/signin/")

        return super().dispatch(request, *args, **kwargs)

#----------------------
#----------------------
#----------------------
#----------------------
#----------------------


# Admin Signin View Class
class AdminSigninView(FormView):
	template_name = "admintemplates/adminsignin.html"
	form_class = SigninForm
	success_url = reverse_lazy("menuapp:adminhome")

	def form_valid(self, form):
		a = form.cleaned_data['username']
		b = form.cleaned_data['password']
		user = authenticate(username=a, password=b)
		if user is not None:
			login(self.request, user)
		else:
			return render(self.request, "admintemplates/adminsignin.html", {
				'error': 'Invalid username or password!!!!',
				'form': form
				})

		return super().form_valid(form)


# Admin  Signout view class
class AdminSignoutView(View):
	def get(self, request):
		logout(request)
		return redirect('/')



# Admin Signup View Class
# class AdminSignupView(FormView):
# 	template_name = "admintemplates/adminsignup.html"
# 	form_class = SignupForm
# 	success_url = "/"

# 	form_valid is defined only on form, create adn update view
# 	def form_valid(self, form):
# 		uname = form.cleaned_data["username"]
# 		email = form.cleaned_data["email"]
# 		pword = form.cleaned_data['password']
# 		print(uname, email, pword)
# 		User.objects.create_user(uname, email, pword)

# 		return super().form_valid(form)




# Admin View Class Start
#-----------------
#-----------------
#-----------------
# Admin Home View
class AdminHomeView(BaseMixin, AdminRequiredMixin, TemplateView):
    template_name = "admintemplates/adminhome.html"
#-----------------
#-----------------
#-----------------


#-----------------
#-----------------
#-----------------
# Admin Banner List View
class AdminBannerListView(BaseMixin, AdminRequiredMixin, ListView):
	template_name = "admintemplates/adminbannerlist.html"
	queryset = SlideBanner.objects.all().order_by("-id")
	context_object_name = "allsliders"
	paginate_by = 8




# Admin Banner Detail View
class AdminBannerCreateView(BaseMixin, AdminRequiredMixin, CreateView):
	template_name = "admintemplates/adminbannercreate.html"
	form_class = SlideBannerForm
	success_url = reverse_lazy('menuapp:adminbannerlist')


# Admin Banner Update View
class AdminBannerUpdateView(BaseMixin, AdminRequiredMixin, UpdateView):
	template_name = "admintemplates/adminbannercreate.html"
	form_class = SlideBannerForm
	model = SlideBanner
	success_url = reverse_lazy('menuapp:adminbannerlist')


# Admin Banner Delete View
class AdminBannerDeleteView(BaseMixin, AdminRequiredMixin, DeleteView):
	template_name = "admintemplates/adminbannerdelete.html"
	model = SlideBanner
	success_url = reverse_lazy('menuapp:adminbannerlist')
#-----------------
#-----------------
#-----------------


#-----------------
#-----------------
#-----------------
# Admin Menu List View
class AdminMenuListView(BaseMixin, AdminRequiredMixin, ListView):
	template_name = "admintemplates/adminmenulist.html"
	queryset = Menu.objects.all().order_by("-id")
	context_object_name = "allsliders"
	paginate_by = 8



# Admin Menu Detail View
class AdminMenuCreateView(BaseMixin, AdminRequiredMixin, CreateView):
	template_name = "admintemplates/adminmenucreate.html"
	form_class = MenuForm
	success_url = reverse_lazy('menuapp:adminmenulist')


# Admin Menu Update View
class AdminMenuUpdateView(BaseMixin, AdminRequiredMixin, UpdateView):
	template_name = "admintemplates/adminmenucreate.html"
	form_class = MenuForm
	model = Menu
	success_url = reverse_lazy('menuapp:adminmenulist')


# Admin Menu Delete View
class AdminMenuDeleteView(BaseMixin, AdminRequiredMixin, DeleteView):
	template_name = "admintemplates/adminmenudelete.html"
	model = Menu
	success_url = reverse_lazy('menuapp:adminmenulist')
#-----------------
#-----------------
#-----------------

#-----------------
#-----------------
#-----------------
# Admin Menu Detail View
class AdminMenuDetailView(BaseMixin, AdminRequiredMixin, DetailView):
	template_name = "admintemplates/adminmenudetail.html"
	model = Menu
	context_object_name = "menuobject"


# Admin Item List View
class AdminItemListView(BaseMixin, AdminRequiredMixin, ListView):
	template_name = "admintemplates/adminitemlist.html"
	queryset = Item.objects.all().order_by("-id")
	context_object_name = "itemobjects"
	paginate_by = 8


# Admin Item Detail View
class AdminItemCreateView(BaseMixin, AdminRequiredMixin, CreateView):
	template_name = "admintemplates/adminitemcreate.html"
	form_class = ItemForm
	success_url = reverse_lazy('menuapp:adminitemlist')


# Admin Item Update View
class AdminItemUpdateView(BaseMixin, AdminRequiredMixin, UpdateView):
	template_name = "admintemplates/adminitemcreate.html"
	form_class = ItemForm
	model = Item
	success_url = reverse_lazy('menuapp:adminitemlist')


# Admin Item Delete View
class AdminItemDeleteView(BaseMixin, AdminRequiredMixin, DeleteView):
	template_name = "admintemplates/adminitemdelete.html"
	model = Item
	success_url = reverse_lazy('menuapp:adminitemlist')




# Admin SpecialItem List View
class AdminSpecialItemListView(BaseMixin, AdminRequiredMixin, ListView):
	template_name = "admintemplates/adminspecialitemlist.html"
	queryset = SpecialItem.objects.all().order_by("-id")
	context_object_name = "specialitemobjects"
	paginate_by = 8


# Admin SpecialItem Detail View
class AdminSpecialItemCreateView(BaseMixin, AdminRequiredMixin, CreateView):
	template_name = "admintemplates/adminspecialitemcreate.html"
	form_class = SpecialItemForm
	success_url = reverse_lazy('menuapp:adminspecialitemlist')


# Admin SpecialItem Update View
class AdminSpecialItemUpdateView(BaseMixin, AdminRequiredMixin, UpdateView):
	template_name = "admintemplates/adminspecialitemcreate.html"
	form_class = SpecialItemForm
	model = SpecialItem
	success_url = reverse_lazy('menuapp:adminspecialitemlist')


# Admin SpecialItem Delete View
class AdminSpecialItemDeleteView(BaseMixin, AdminRequiredMixin, DeleteView):
	template_name = "admintemplates/adminspecialitemdelete.html"
	model = SpecialItem
	success_url = reverse_lazy('menuapp:adminspecialitemlist')



# Admin SpecialItem List View
class AdminStaffListView(BaseMixin, AdminRequiredMixin, ListView):
	template_name = "admintemplates/adminstafflist.html"
	queryset = Staff.objects.all().order_by("-id")
	context_object_name = "staffobjects"
	paginate_by = 8


# Admin SpecialItem Detail View
class AdminStaffCreateView(BaseMixin, AdminRequiredMixin, CreateView):
	template_name = "admintemplates/adminstaffcreate.html"
	form_class = StaffForm
	success_url = reverse_lazy('menuapp:adminstafflist')


# Admin SpecialItem Update View
class AdminStaffUpdateView(BaseMixin, AdminRequiredMixin, UpdateView):
	template_name = "admintemplates/adminstaffcreate.html"
	form_class = StaffForm
	model = Staff
	success_url = reverse_lazy('menuapp:adminstafflist')


# Admin SpecialItem Delete View
class AdminStaffDeleteView(BaseMixin, AdminRequiredMixin, DeleteView):
	template_name = "admintemplates/adminstaffdelete.html"
	model = Staff
	success_url = reverse_lazy('menuapp:adminstafflist')






# Admin About View
class AdminAboutView(BaseMixin, AdminRequiredMixin, TemplateView):
    template_name = "admintemplates/adminabout.html"


# Admin Gallery View
class AdminGalleryView(BaseMixin, AdminRequiredMixin, TemplateView):
    template_name = "admintemplates/admingallery.html"



# Admin Basket View
class AdminBasketListView(BaseMixin, AdminRequiredMixin, ListView):
	template_name = "admintemplates/adminbasketlist.html"
	queryset = Basket.objects.all().order_by("-id")
	context_object_name = 'allbaskets'


