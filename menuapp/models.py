from django.db import models
from django.contrib.auth.models import User, Group
from .constants import *




class TimeStamp(models.Model):
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True, null = True, blank = True)
	active = models.BooleanField(default = True)

	class Meta: 
		abstract = True



class Restaurant(TimeStamp):
	name = models.CharField(max_length = 300)
	logo = models.ImageField(upload_to = "restro")
	email = models.EmailField()
	phone = models.CharField(max_length = 50)
	mobile = models.CharField(max_length = 50, null = True, blank = True)
	address = models.CharField(max_length = 200)
	about = models.TextField()
	profile_image = models.ImageField(upload_to = "restro")
	website = models.CharField(max_length = 100)
	map_location = models.CharField(max_length = 500)
	favicon = models.ImageField(upload_to = "restro")
	facebook = models.CharField(max_length = 300, null = True)
	instagram = models.CharField(max_length = 300, null = True)
	estb_date = models.DateField()

	def __str__(self):
		return self.name



class SlideBanner(TimeStamp):
	title = models.CharField(max_length = 300)
	caption = models.TextField()
	link = models.CharField(max_length = 100)

	def __str__(self):
		return self.title



class Menu(TimeStamp):
	title = models.CharField(max_length = 100)
	caption = models.CharField(max_length = 300, null = True, blank = True)

	def __str__(self):
		return self.title


class Item(TimeStamp):
	name = models.CharField(max_length = 300)
	menu = models.ForeignKey(Menu, on_delete = models.CASCADE)
	photo = models.ImageField(upload_to = "item")
	price = models.DecimalField(max_digits = 30,decimal_places=2, null = True, blank = True)

	def __str__(self):
		return self.name



class SpecialItem(TimeStamp):
	name = models.CharField(max_length = 300)
	photo = models.ImageField(upload_to = "item")
	price = models.IntegerField(null = True, blank = True)
	caption = models.CharField(max_length = 500, null = True, blank = True)

	def __str__(self):
		return self.name



class Staff(TimeStamp):
	name = models.CharField(max_length = 300)
	photo = models.ImageField(upload_to = "staff")
	position = models.CharField(max_length = 200)
	experience = models.IntegerField(null = True, blank = True)

	def __str__(self):
		return self.name



class Feedback(TimeStamp):
	name = models.CharField(max_length = 300)
	email = models.EmailField(null = True, blank = True)
	message = models.TextField()

	def __str__(self):
		return self.name



class Enquery(TimeStamp):
	name = models.CharField(max_length = 300)
	email = models.EmailField(null = True, blank = True)
	mobile = models.CharField(max_length=200)
	menu = models.ForeignKey(Menu, on_delete = models.CASCADE, null=True, blank=True)
	item = models.ForeignKey(Item, on_delete = models.CASCADE, null=True, blank=True)
	message = models.TextField()

	def __str__(self):
		return self.name




class Subscriber(TimeStamp):
    email = models.EmailField()

    def __str__(self):
        return self.email



class Album(TimeStamp):
	title = models.CharField(max_length=200)
	profile_image = models.ImageField(upload_to='image', null=True, blank=True)
	details = models.TextField()

	def __str__(self):
		return self.title


class AlbumImage(TimeStamp):
	album = models.ForeignKey(Album, on_delete=models.CASCADE)
	image = models.ImageField(upload_to="image")
	caption = models.CharField(max_length=200, null=True, blank=True)

	def __str__(self):
		return self.caption



class AlbumVideo(TimeStamp):
	title = models.CharField(max_length=200)
	link = models.CharField(max_length=300)
	description = models.TextField()

	def __str__(self):
		return self.title




class Admin(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	mobile = models.CharField(max_length = 300)
	name = models.CharField(max_length = 300)
	address = models.CharField(max_length = 300)
	photo = models.ImageField(upload_to = 'user')

	def __str__(self):
		return self.name



class Customer(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	mobile = models.CharField(max_length = 300)
	name = models.CharField(max_length = 300)
	address = models.CharField(max_length = 300, null = True, blank = True)
	photo = models.ImageField(upload_to = 'user')

	def save(self, *args, **kwargs):
		grp, created = Group.objects.get_or_create(name ="customer")
		self.user.groups.add(grp)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.name


class Basket(TimeStamp):
	customer = models.ForeignKey(Customer, on_delete= models.CASCADE, null=True, blank=True)
	total = models.DecimalField(max_digits=30, decimal_places=2)

	def __str__(self):
		return "Basket id: " + str(self.id)


class ItemBasket(TimeStamp):
	basket = models.ForeignKey(Basket, on_delete=models.CASCADE)
	item = models.ForeignKey(Item, on_delete=models.CASCADE)
	rate = models.DecimalField(max_digits=20, decimal_places=2)
	quantity = models.PositiveIntegerField()
	subtotal = models.DecimalField(max_digits=20, decimal_places=2)

	def __str__(self):
		return self.item.name + "(" + str(self.basket) + ")"



class Order(TimeStamp):
	basket = models.OneToOneField(Basket, on_delete=models.CASCADE)
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
	subtotal = models.DecimalField(max_digits=20, decimal_places=2)
	discount = models.DecimalField(max_digits=20, decimal_places=2)
	total = models.DecimalField(max_digits=20, decimal_places=2)
	deliver_date = models.DateField(null=True, blank=True)
	municipality = models.CharField(max_length = 200, null=True, blank=True)
	street_address = models.CharField(max_length = 200, null=True, blank=True)
	alt_mobile = models.CharField(max_length = 200, null=True, blank=True)
	order_status = models.CharField(max_length=50, choices=ORDER_STATUS)
	payment_method = models.CharField(max_length=50, choices = PAYMENT_METHOD)


	def __str__(self):
		return "Order id: " + str(self.id)



