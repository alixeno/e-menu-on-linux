from django import forms
from .models import *
from django.contrib.auth.models import User



class FeedbackForm(forms.ModelForm):
	class Meta:
		model = Feedback
		fields = "__all__"
		widgets = {
		'name': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter your name...'
			}),
		'email': forms.EmailInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter your email...'
			}),
		'message': forms.Textarea(attrs={
			'class': 'form-control',
			'placeholder': 'Enter your message...'
			})
		}




class EnqueryForm(forms.ModelForm):
	item = forms.ModelChoiceField(widget=forms.Select(),
		queryset = Item.objects.none())
	class Meta:
		model = Enquery
		fields = "__all__"
		widgets = {
		'name': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter your name...'
			}),
		'email': forms.EmailInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter your email...'
			}),
		'message': forms.Textarea(attrs={
			'class': 'form-control',
			'placeholder': 'Enter your queries...'
			}),
		'menu': forms.Select(attrs={
			'class': 'form-control',
			}),
		'item': forms.Select(attrs={
			'class': 'form-control',
			}),
		'mobile': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter mobile number...'
			})
		}

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		if 'menu' in self.data:
			menu = self.data.get('menu', None)
			if menu:
				self.fields['item'].queryset = Item.objects.filter(menu=menu)
		







class SigninForm(forms.Form):
	username = forms.CharField(widget = forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'Enter your username...'
		}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={
		'class': 'form-control',
		'placeholder': 'Enter your password...'
		}))



#creation of class for form (signup)
class CustomerSignupForm(forms.ModelForm):
	username = forms.CharField(widget= forms.TextInput)
	email = forms.EmailField(widget = forms.EmailInput)
	password = forms.CharField(widget = forms.PasswordInput)
	confirm_password = forms.CharField(widget = forms.PasswordInput)
	class Meta:
		model = Customer
		fields = ['username', 'email', 'password', 'confirm_password', 'name', 'mobile', 'address', 'photo']
		#class vitra method lekhne for validation
		#first validation check for username
		def clean_username(self):
			uname = self.cleaned_data["username"]
			#cleaned_data le form ko euta field ko data line garxa
			#filter is used to query if such name exists in our database
			if User.objects.filter(username = uname).exists():
				raise forms.ValidationError("User with this username already exists")

			return uname

		#second validation check for email
		def clean_email(self):
			eemail = self.cleaned_data["email"]
			if User.objects.filter(email = eemail).exists():
				raise forms.ValidationError("Email with this email address already exists")
			return eemail

		def clean_confirm_password(self):
			password = self.cleaned_data["password"]
			c_pword = self.cleaned_data["confirm_password"]
			if password != c_pword:
				raise forms.ValidationError("Password didn't match")

			return c_pword



class SlideBannerForm(forms.ModelForm):
	class Meta:
		model = SlideBanner
		fields = "__all__"
		widgets = {
		'title': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter title'
			}),
		'caption': forms.Textarea(attrs={
			'class': 'form-control',
			'placeholder': 'Enter caption...'
			}),
		'link': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter link...'
			})
		}


class MenuForm(forms.ModelForm):
	class Meta:
		model = Menu
		fields = "__all__"
		widgets = {
		'title': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter title'
			}),
		'caption': forms.Textarea(attrs={
			'class': 'form-control',
			'placeholder': 'Enter caption...'
			}),
		}



class ItemForm(forms.ModelForm):
	class Meta:
		model = Item
		fields = "__all__"
		widgets = {
		'name': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter title'
			}),
		'price': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter price...'
			}),
		'photo': forms.ClearableFileInput(attrs={
			'class': 'form-control'
			}),
		'menu': forms.Select(attrs={
			'class': 'form-control'
			})
		}


class SpecialItemForm(forms.ModelForm):
	class Meta:
		model = SpecialItem
		fields = "__all__"
		widgets = {
		'name': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter title'
			}),
		'caption': forms.Textarea(attrs={
			'class': 'form-control',
			'placeholder': 'Enter caption...'
			}),
		'price': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter price...'
			}),
		'photo': forms.ClearableFileInput(attrs={
			'class': 'form-control'
			}),
		}


class StaffForm(forms.ModelForm):
	class Meta:
		model = Staff
		fields = "__all__"
		widgets = {
		'name': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter title'
			}),
		'position': forms.Textarea(attrs={
			'class': 'form-control',
			'placeholder': 'Enter position...'
			}),
		'experience': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter experience...'
			}),
		'photo': forms.ClearableFileInput(attrs={
			'class': 'form-control'
			}),
		}



class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = "__all__"
        widgets = {
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter email...'
            }),
        }



class OrderForm(forms.ModelForm):
	class Meta:
		model = Order
		fields = ["deliver_date", 'payment_method', 'municipality', 'street_address', 'alt_mobile']