

ORDER_STATUS = (
	("orderplaced", "Order Placed"),
	("orderprocessing", "Order Processing"),
	("ontheway", "On The Way"),
	("delivered", "Delivered"),
	)


PAYMENT_METHOD = (
	("cashondeliver", "Cash On Delivery"),
	("cardondeliver", "Card On Delivery"),
	("epayment", "E-Payment"),
	)


