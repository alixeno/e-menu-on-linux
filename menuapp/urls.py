from django.urls import path
from .views import *

app_name = "menuapp"

urlpatterns = [

    # Client Page Urls Start
    path("", ClientHomeView.as_view(), name="clienthome"),
    path("gallery/", ClientGalleryView.as_view(), name="clientgallery"),
    path("service/", ClientServiceView.as_view(), name="clientservice"),

    path("enquery/", ClientEnqueryView.as_view(), name="clientenquery"),

    path("ajax/menu/", 
        AjaxGetItemsView.as_view(), name = "ajaxgetitems"),




    path("feedback/", ClientFeedbackView.as_view(), name="clientfeedback"),
    path("item-all/", ClientItemView.as_view(), name="clientitem"),
    # path("sidebar/", ClientSidebarView.as_view(), name="clientsidebar"),
	
	path("menu/<int:pk>/detail/", ClientMenuDetailView.as_view(), name= "clientmenudetail"),
	path("menu/specialitem/list/", 
		ClientSpecialItemListView.as_view(), name = "clientspecialitemlist"),


    path("search/", SearchView.as_view(), name = "search"),

    path("subscriber/", SubscriberCreateView.as_view(), name="clientsubscriber"),


    path('album/', ClientAlbumView.as_view(), name='clientalbum'),
    path("album/<int:pk>/detail", ClientAlbumDetailView.as_view(),name="clientalbumdetail"),


    path("add-to-basket/<int:pk>/", AddToBasketView.as_view(), name = "addtobasket"),

    path("basket/", BasketView.as_view(), name = 'basket'),
    path("order/", OrderCreateView.as_view(), name = "ordercreate"),
    path("order/list/", OrderListView.as_view(), name = 'orderlist'),
    path("order/<int:pk>/detail/", OrderDetailView.as_view(), name = 'orderdetail'),


    path("manage-basket/<int:pk>/<action>/", 
        ManageBasketView.as_view(), name = 'managebasket'),

    path("empty-basket/", 
        EmptyBasketView.as_view(), name = 'emptybasket'),

    path("customer/sign-up/", CustomerSignupView.as_view(), name = 'customersignup'),
    path("customer/login/", CustomerLoginView.as_view(), name = 'customerlogin'),







#--------------
#--------------
#--------------
#--------------
#--------------
#--------------

#----------------------
#----------------------
#----------------------
#----------------------
#----------------------


	# Admin Signin Form
	path("emenu-admin/signin/", AdminSigninView.as_view(), name = "adminsignin"),
	path("emenu-admin/signout/", AdminSignoutView.as_view(), name = "adminsignout"),
    # path("emenu-admin/signup/", AdminSignupView.as_view(), name = "adminsignup"),


# Admin Home View
    path("emenu-admin/home/", AdminHomeView.as_view(), name="adminhome"),

# Admin Banner Views
    path("emenu-admin/banner/list/", AdminBannerListView.as_view(), name="adminbannerlist"),
    path("emenu-admin/banner/create/", AdminBannerCreateView.as_view(), name="adminbannercreate"),
    path("emenu-admin/banner/<int:pk>/update/", AdminBannerUpdateView.as_view(), name="adminbannerupdate"),
    path("emenu-admin/banner/<int:pk>/delete/", AdminBannerDeleteView.as_view(), name="adminbannerdelete"),

# Admin Menu Views
    path("emenu-admin/menu/list/", AdminMenuListView.as_view(), name="adminmenulist"),
    path("emenu-admin/menu/create/", AdminMenuCreateView.as_view(), name="adminmenucreate"),
    path("emenu-admin/menu/<int:pk>/update/", AdminMenuUpdateView.as_view(), name="adminmenuupdate"),
    path("emenu-admin/menu/<int:pk>/delete/", AdminMenuDeleteView.as_view(), name="adminmenudelete"),

# Admin Menu Category Views
    path("emenu-admin/menu/<int:pk>/list/", AdminMenuDetailView.as_view(), name="adminmenudetail"),

# Admin All Items List View
    path("emenu-admin/items/list/", AdminItemListView.as_view(), name="adminitemlist"),
    path("emenu-admin/items/create/", AdminItemCreateView.as_view(), name="adminitemcreate"),
    path("emenu-admin/items/<int:pk>/update/", AdminItemUpdateView.as_view(), name="adminitemupdate"),
    path("emenu-admin/items/<int:pk>/delete/", AdminItemDeleteView.as_view(), name="adminitemdelete"),

# Admin All Special Items List View
    path("emenu-admin/specialitems/list/", AdminSpecialItemListView.as_view(), name="adminspecialitemlist"),
    path("emenu-admin/specialitems/create/", AdminSpecialItemCreateView.as_view(), name="adminspecialitemcreate"),
    path("emenu-admin/specialitems/<int:pk>/update/", 
        AdminSpecialItemUpdateView.as_view(), name="adminspecialitemupdate"),
    path("emenu-admin/specialitems/<int:pk>/delete/", 
        AdminSpecialItemDeleteView.as_view(), name="adminspecialitemdelete"),


# Admin Staff Urls
    path("emenu-admin/staff/list/", AdminStaffListView.as_view(), name="adminstafflist"),
    path("emenu-admin/staff/create/", AdminStaffCreateView.as_view(), name="adminstaffcreate"),
    path("emenu-admin/staff/<int:pk>/update/", 
        AdminStaffUpdateView.as_view(), name="adminstaffupdate"),
    path("emenu-admin/staff/<int:pk>/delete/", 
        AdminStaffDeleteView.as_view(), name="adminstaffdelete"),

    path("emenu-admin/about/", AdminAboutView.as_view(), name="adminabout"),
    path("emenu-admin/gallery/", AdminGalleryView.as_view(), name="admingallery"),


    path("emenu-admin/basket/list/", AdminBasketListView.as_view(), name = 'adminbasketlist'),








########### Sericalizers for making api
    path("json_api/staff/list-create/", StaffApiView.as_view(), name = 'staffapi'),
]