# Generated by Django 2.2.3 on 2019-08-08 05:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('menuapp', '0012_auto_20190802_2012'),
    ]

    operations = [
        migrations.CreateModel(
            name='Basket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('total', models.DecimalField(decimal_places=2, max_digits=30)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('subtotal', models.DecimalField(decimal_places=2, max_digits=20)),
                ('discount', models.DecimalField(decimal_places=2, max_digits=20)),
                ('total', models.DecimalField(decimal_places=2, max_digits=20)),
                ('deliver_date', models.DateField(blank=True, null=True)),
                ('order_status', models.CharField(choices=[('orderplaced', 'Order Placed'), ('orderprocessing', 'Order Processing'), ('ontheway', 'On The Way'), ('delivered', 'Delivered')], max_length=50)),
                ('payment_method', models.CharField(choices=[('cashondeliver', 'Cash On Delivery'), ('cardondeliver', 'Card On Delivery'), ('epayment', 'E-Payment')], max_length=50)),
                ('basket', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='menuapp.Basket')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ItemBasket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('rate', models.DecimalField(decimal_places=2, max_digits=20)),
                ('quantity', models.PositiveIntegerField()),
                ('subtotal', models.DecimalField(decimal_places=2, max_digits=20)),
                ('basket', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='menuapp.Basket')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='menuapp.Item')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('mobile', models.CharField(max_length=300)),
                ('name', models.CharField(max_length=300)),
                ('address', models.CharField(blank=True, max_length=300, null=True)),
                ('photo', models.ImageField(upload_to='user')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='basket',
            name='customer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='menuapp.Customer'),
        ),
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('mobile', models.CharField(max_length=300)),
                ('name', models.CharField(max_length=300)),
                ('address', models.CharField(max_length=300)),
                ('photo', models.ImageField(upload_to='user')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
