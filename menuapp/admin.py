from django.contrib import admin
from .models import *


admin.site.register([Restaurant,Menu, Item, SpecialItem, SlideBanner,
					 Staff, Feedback, Enquery, Subscriber, Album, AlbumImage, AlbumVideo,
					 Basket, Order, ItemBasket, Customer, Admin])